# `spade_client` project #

The `spade_client` project contains both the `spade_client` package that
provides a python interface to a SPADE server, and the `spade-cli` command line
interface that uses that package to provide command line access to the server.
