"""
This module provides the classes and functions that collaborate to
create a Command Line Interface for **SPADE** clients.
"""

from .response_error import ResponseError, EmptyResponseError
from .spade import SPADE
from .spade_request import SpadeRequest
from .spade_request_constraints import SpadeRequestConstraints
from .timestamp_request import TimeStampRequest
