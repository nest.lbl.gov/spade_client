"""
Defines the ResponseError class
"""

from typing import Optional, Union


class ResponseError(Exception):
    """
    This class captures the response to failed a request to SPADE
    """

    def __init__(
        self, message: str, errorCode: int, response: Optional[Union[bytes, str]]
    ):
        self.code = errorCode
        self.message = message
        self.response = response


class EmptyResponseError(ResponseError):
    """
    This class captures the response that is empty
    """

    def __init__(self, message: str, errorCode: int):
        ResponseError.__init__(self, message, errorCode, None)
