"""
Defines the SPADE class
"""

from typing import Callable, Optional, Union

import logging
import os
import xml.dom.minidom
import xml.etree.ElementTree as ET

import requests

from .response_error import EmptyResponseError, ResponseError

_DEBUG_SEPARATOR = "--------"
_HEADERS = {"Content-Type": "application/xml", "Accept": "application/xml"}


def _check_status(url: str, response: requests.Response, expected: int) -> None:
    """Checks the return status of a request to a URL

    Args:
        url: the URL to which the request was made
        response: the response to the request
        expected: the expected response code
    """
    if expected == response.status_code:
        return
    if 400 == response.status_code:
        raise ResponseError(
            'Application at "' + url + '" can not process this request as it is bad',
            response.status_code,
            response.text,
        )
    if 401 == response.status_code:
        raise ResponseError(
            'Not authorized to execute commands for Application at "' + url,
            response.status_code,
            response.text,
        )
    if 404 == response.status_code:
        raise ResponseError(
            'Application at "' + url + '" not found',
            response.status_code,
            response.text,
        )
    raise ResponseError(
        "Unexpected status ("
        + str(response.status_code)
        + ') returned from "'
        + url
        + '"',
        response.status_code,
        response.text,
    )


class SPADE:
    """
    This class provides programatic access to a SPADE instance
    """

    def __init__(  # pylint: disable = too-many-arguments
        self,
        url: str = "http://localhost:8080/spade/local/report/",
        dump: bool = False,
        cert: Optional[str] = None,
        key: Optional[str] = None,
        cacert: Optional[str] = None,
        verify: Optional[bool] = True,
    ):
        """
        Creates an instance of this class.

        Args:
            url: the URL of the SPADE server.
            dump: True if the raw XML exchanges should be dumped.
            cert: path to the file containing the client's x509
                certificate.
            key: path to the file containing path to the client's
                private x509 key.
            cacert: path to the file containing one or more CA
                x509certificates.
            verify:
                True when host in x509 certificate must be valid.
        #: presented by the server, and will ignore hostname mismatches
        """
        self.url = url
        self.debug = dump
        self.session = requests.Session()
        home_dir = os.getenv("HOME")
        if None is home_dir:
            client_dir = "/.spade/client"
        else:
            client_dir = home_dir + "/.spade/client"
        if None is cert:
            cert = client_dir + "/cert/spade_client.pem"  # Client certificate
        if None is key:
            key = client_dir + "/private/spade_client.key"  # Client private key
        if os.path.exists(cert) and os.path.exists(key):
            self.session.cert = (cert, key)
        if None is cacert:
            cacert = client_dir + "/cert/cacert.pem"  # CA certificate file
        if os.path.exists(cacert):
            logging.warning(
                "CACERT not currently supported - this option will be ignored"
            )
        #            self.session.cacert = cacert
        if None is not verify:
            self.session.verify = verify

    def debug_separator(self) -> None:
        """
        Outputs the debug separator.
        """
        logging.debug(_DEBUG_SEPARATOR)

    def get_application(self) -> ET.Element:
        """
        Returns:
            the application document at the URL

        Raises:
            ResponseError: if the server response in not OK.
        """

        try:
            response = self.session.get(self.url)
            _check_status(self.url, response, 200)
            application = ET.fromstring(response.text)
            self.__pretty_print(self.url, ET.tostring(application))
            return application
        except requests.exceptions.ConnectionError as connection_error:
            raise ResponseError(
                'Unable to conenct to application at "' + self.url + '"',
                0,
                str(connection_error),
            ) from connection_error

    def execute_named_resource(
        self,
        application: ET.Element,
        xpath: str,
        name: str,
        callback: Callable[[ET.Element], ET.Element],
    ) -> ET.Element:
        """
        Executes, if found, the named resource using the specified
        action method.

        Returns:
            True is the resource has been found and executed.

        Raises:
            ResponseError: if the server response in not OK.
        """
        resource = self._get_named_resource_url(application, xpath, name)
        if None is resource:
            raise ValueError(
                f'No ressource in the "{xpath}" element has the name "{name}"'
            )
        return callback(resource)

    def _get_named_resource_url(
        self, application: ET.Element, xpath: str, name: str
    ) -> Optional[ET.Element]:
        """
        Returns the URI of the application resource if the supplied name
        is an application_action's name.

        Args:
            xpath: the xpath to the Named Resources within the Named
                Resource group to be searched.
            name: the name of the resource whose URL should be
                returned.

        Returns:
            the URI of a Named Resource.

        Raises:
            ResponseError: if the server response in not OK.
        """
        resources_element = application.findall(xpath)
        for resource in resources_element:
            name_element = resource.find("name")
            if None is not name_element and name == name_element.text:
                return resource
        return None

    def get_response(self, uri: str, attachment: Optional[ET.Element]) -> ET.Element:
        """
        Executes an HTTP GET on the supplied URL using the attachement, if
        any, as its document.

        Raises:
            EmptyResponseError: when the response contains no text.
        """
        logging.debug("Response requested for URL: %s", uri)
        if None is attachment:
            response = self.session.get(uri, headers=_HEADERS)
        else:
            self.__pretty_print(uri, ET.tostring(attachment), False)
            response = self.session.get(
                uri, data=ET.tostring(attachment), headers=_HEADERS
            )
        _check_status(uri, response, 200)
        if None is response.text:
            raise EmptyResponseError(
                'Resource at "' + uri + '" returned response with no text',
                response.status_code,
            )
        result = ET.fromstring(response.text)
        self.__pretty_print(uri, ET.tostring(result))
        return result

    def __pretty_print(
        self,
        uri: Union[bytes, str],
        document: Union[bytes, str],
        is_response: bool = True,
    ) -> None:
        """
        Prints out a formatted version of the supplied XML

        Args:
            uri: the URL to which the request was made.
            document: the XML document to print.
            is_response: True is the XML is the reponse to a request.
        """
        if self.debug:
            if None is not uri:
                if is_response:
                    logging.debug("URL : Response : %s", str(uri))
                else:
                    logging.debug("URL : Request :  %s", str(uri))
            logging.debug(xml.dom.minidom.parseString(document).toprettyxml())
            self.debug_separator()
