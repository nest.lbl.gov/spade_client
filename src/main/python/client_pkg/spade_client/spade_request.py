"""
Declares a SpadeRequest class
"""

from typing import Optional

import xml.etree.ElementTree as ET

from .response_error import ResponseError
from .spade import SPADE


class SpadeRequest:  # pylint: disable = too-few-public-methods
    """
    This class is the base class for sending any requests to a SPADE server.
    """

    def __init__(self, spade: SPADE):
        """
        Creates an instance of this class.
        """
        self.__spade = spade

    def _callback(self, resource: ET.Element) -> ET.Element:
        """
        Uses the provided resource to fulfill this objects responsibilities.


        Raises:
            ResponseError: when the resource has no uri.
        """
        uri_element = resource.find("uri")
        if None is uri_element or None is uri_element.text:
            raise ResponseError("Resource has not uri", 0, None)
        uri = uri_element.text
        extension = self._get_extension()  # pylint: disable=assignment-from-none
        if None is extension:
            extended_uri = uri
        else:
            extended_uri = uri + "/" + extension
        query = self._get_query_string()  # pylint: disable=assignment-from-none
        if None is query:
            uri_to_use = extended_uri
        else:
            uri_to_use = extended_uri + "?" + query
        attachment_element = resource.find("attachment")
        document: Optional[ET.Element]
        if None is attachment_element or None is attachment_element.text:
            document = None
        else:
            document = self._prepare_document(  # pylint: disable= assignment-from-none
                attachment_element.text
            )
        return self.__spade.get_response(uri_to_use, document)

    def execute(self) -> ET.Element:
        """
        Executes this object's request.
        """
        return self.__spade.execute_named_resource(
            self.__spade.get_application(),
            self._get_group_xpath(),
            self._get_resource_name(),
            self._callback,
        )

    def _get_extension(self) -> Optional[str]:
        """
        Returns any extension that should be added to the URI of the
        resource.
        """
        return None

    def _get_group_xpath(self) -> str:
        """
        Returns the XPath to the group of timing actions.
        """
        return ""

    def _get_query_string(self) -> Optional[str]:
        """
        Returns the query string associated with this request.
        """
        return None

    def _get_resource_name(self) -> str:
        """
        Returns the name of the resource to used for this request.
        """
        return ""

    def _prepare_document(self, _: str) -> Optional[ET.Element]:
        """
        Prepares the document, if any, to be used as the attachement for
        this request.

        Args:
            media_type: the type of media in which the document should be prepared.
        """
        return None
