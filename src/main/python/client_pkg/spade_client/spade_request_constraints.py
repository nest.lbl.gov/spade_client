"""
Defines the SpadeRequestConstraints class
"""

from typing import List, Optional

from datetime import datetime


class SpadeRequestConstraints:  # pylint: disable = too-few-public-methods
    """
    This class encapsulates conditions to apply to a spade requests.
    """

    def __init__(self) -> None:
        """
        Creates an instance of this class.
        """
        self.after: Optional[datetime] = None
        self.before: Optional[datetime] = None
        self.maximum: Optional[int] = None
        self.reverse: Optional[bool] = None
        self.direction: Optional[str] = None
        self.neighbor: Optional[str] = None
        self.registrations: Optional[List[str]] = None

    def get_query_string(self):
        """
        Prepares a query to be used with a TimeStamp URI
        """
        query_string = ""
        conjunction = ""
        if None is not self.maximum:
            query_string = query_string + conjunction + "max=" + str(self.maximum)
            conjunction = "&"
        if None is not self.after:
            query_string = (
                query_string
                + conjunction
                + "after="
                + self.after.isoformat().replace("+", "%2B")
            )
            conjunction = "&"
        if None is not self.before:
            query_string = (
                query_string
                + conjunction
                + "before="
                + self.before.isoformat().replace("+", "%2B")
            )
            conjunction = "&"
        if None is not self.reverse:
            query_string = query_string + conjunction + "reverse=" + str(self.reverse)
            conjunction = "&"
        if None is not self.direction:
            query_string = query_string + conjunction + "direction=" + self.direction
            conjunction = "&"
        if None is not self.neighbor:
            query_string = query_string + conjunction + "neighbor=" + self.neighbor
            conjunction = "&"
        if None is not self.registrations:
            for registration in self.registrations:  # pylint: disable=not-an-iterable
                query_string = (
                    query_string + conjunction + "registration=" + registration
                )
                conjunction = "&"
        if "" == query_string:
            return None
        return query_string
