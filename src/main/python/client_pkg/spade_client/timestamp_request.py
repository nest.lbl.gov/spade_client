"""
Declares a TimeStampRequest class
"""

from typing import Optional

from .spade import SPADE
from .spade_request import SpadeRequest
from .spade_request_constraints import SpadeRequestConstraints


class TimeStampRequest(SpadeRequest):  # pylint: disable= too-few-public-methods
    """
    This class encapsulates a request from timestamp information.
    """

    def __init__(
        self,
        spade: SPADE,
        resource_name: str,
        query: Optional[SpadeRequestConstraints] = None,
        extension: Optional[str] = None,
    ):
        """
        Create an instance of this class.
        """
        SpadeRequest.__init__(self, spade)
        self.__extension = extension
        self.__query = query
        self.__resource_name = resource_name

    def _get_extension(self) -> Optional[str]:
        """
        Returns any extension that should be added to the URI of the
        resource.
        """
        return self.__extension

    def _get_group_xpath(self) -> str:
        """
        Returns the XPath to the group of timing actions.
        """
        return 'reports/[name="timing"]/action'

    def _get_query_string(self) -> Optional[str]:
        """
        Returns the query string associated with this request.
        """
        if None is self.__query:
            return None
        return self.__query.get_query_string()

    def _get_resource_name(self) -> str:
        """
        Returns the name of the resource to used for this request.
        """
        return self.__resource_name
