"""
Short example of how to request the list of confirmables.
"""

import os
import xml.dom.minidom
import xml.etree.ElementTree as ET
from spade_client import SPADE, SpadeRequestConstraints, TimeStampRequest

url = os.getenv("SPADE_APPLICATION", "http://localhost:8080/spade/local/report/")
spade = SPADE(url)
request_constraints = SpadeRequestConstraints()
request_constraints.maximum = 4
confirmable_request = TimeStampRequest(spade, "confirmable", request_constraints)
digest = confirmable_request.execute()
print(xml.dom.minidom.parseString(ET.tostring(digest)).toprettyxml())
